package paliaplus.log;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;

import org.deckfour.xes.out.XSerializer;
import org.deckfour.xes.out.XesXmlSerializer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import paliaplus.log.PaliaActivity;
import paliaplus.log.PaliaLog;
import paliaplus.log.PaliaSample;

public class PaliaActivityTest {

	PaliaActivity testActivity;
	XSerializer serializer;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testActivity = new PaliaActivity();
		assertNotNull(testActivity);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		serializer = new XesXmlSerializer();
		testActivity.setStartTime(new Date());
		testActivity.setEndTime(new Date());
		PaliaLog log = new PaliaLog();
		PaliaSample sample = new PaliaSample();
		sample.add(testActivity);
		log.add(sample);
		try{
			serializer.serialize(log, System.out);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
