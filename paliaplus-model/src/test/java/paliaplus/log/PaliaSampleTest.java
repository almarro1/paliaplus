package paliaplus.log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import paliaplus.log.PaliaActivity;
import paliaplus.log.PaliaLog;
import paliaplus.log.PaliaSample;
import paliaplus.test.utils.TestUtils;

public class PaliaSampleTest {
	
	PaliaSample testSample;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testSample = new PaliaSample();
		assertNotNull(testSample);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addActivity(){
		PaliaActivity activity = new PaliaActivity();
		activity.setStartTime(new Date());
		assertEquals(0, testSample.size());
		
		testSample.addActivity(activity);
		
		assertEquals(1, testSample.size());
	}
	
	@Test
	public void addOrderedActivity(){
		Date d = new Date();
		PaliaActivity activity1 = new PaliaActivity();
		activity1.setStartTime(d);
		activity1.setSampleId(testSample.getId());
		PaliaActivity activity2 = new PaliaActivity();
		activity2.setStartTime(new Date(d.getTime()+10));
		activity2.setSampleId(testSample.getId());
		PaliaActivity activity3 = new PaliaActivity();
		activity3.setStartTime(new Date(d.getTime()-10));
		activity3.setSampleId(testSample.getId());
		
		assertEquals(0, testSample.size());
		testSample.insertOrdered(activity1);
		testSample.insertOrdered(activity2);
		testSample.insertOrdered(activity3);
		assertEquals(3, testSample.size());
		
		PaliaLog log = new PaliaLog();
		log.addSample(testSample);
		TestUtils.printLog(log);
		
		PaliaActivity[] activities = testSample.toArray(new PaliaActivity[0]);
		assertNotNull(activities);
		assertEquals(3, activities.length);
		assertNotNull(activities[0]);
		assertEquals(d.getTime()-10, activities[0].getStartTime().getTime());
		assertEquals(d.getTime(), activities[1].getStartTime().getTime());
		assertEquals(d.getTime()+10, activities[2].getStartTime().getTime());
	}

}
