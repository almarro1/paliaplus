package paliaplus.log.reader;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import paliaplus.log.PaliaLog;
import paliaplus.log.reader.SpheraJSONReader;
import paliaplus.test.utils.TestUtils;

public class TestSpheraJSONReader {
	
	SpheraJSONReader reader;
	PaliaLog log;
	
	static double prevMem, postMem;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.gc();
		prevMem = TestUtils.getMemUsage();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		postMem = TestUtils.getMemUsage();
		System.out.println("Memory increase (kb): "+(postMem-prevMem));
		prevMem = postMem;
		System.gc();
		postMem = TestUtils.getMemUsage();
		System.out.println("Possible memory leak (kb): "+(postMem-prevMem));
	}

	@Before
	public void setUp() throws Exception {
		reader = new SpheraJSONReader();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReadFromFileName() throws FileNotFoundException {
		System.out.println("Working dir: "+System.getProperty("user.dir"));
		String fileName ="./src/test/resources/processminingcorpus.json";
		log = reader.parse(fileName);
		assertNotNull(log);
		//TestUtils.printLog(log);
	}

}
