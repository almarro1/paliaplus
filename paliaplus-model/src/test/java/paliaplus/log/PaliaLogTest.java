package paliaplus.log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeBoolean;
import org.deckfour.xes.model.XAttributeContainer;
import org.deckfour.xes.model.XAttributeContinuous;
import org.deckfour.xes.model.XAttributeDiscrete;
import org.deckfour.xes.model.XAttributeID;
import org.deckfour.xes.model.XAttributeList;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.xstream.XesXStreamPersistency;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;

import paliaplus.log.PaliaLog;
import paliaplus.test.utils.TestUtils;

public class PaliaLogTest {
	
	private PaliaLog testLog;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testLog = new PaliaLog();
		assertNotNull(testLog);
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testSetCorpusId(){
		String id = UUID.randomUUID().toString();
		assertEquals("Expected 2 attributes",2, testLog.getAttributes().size());
		testLog.setCorpusId(id);
		
		assertEquals("There should be exactly 2 attribute",2, testLog.getAttributes().size());
		
		XAttribute corpusId = testLog.getAttributes().get(PaliaLog.CORPUS_ID);

		assertNotEquals("Retrieved corpusId should contain a valid UUID value","",corpusId);
		assertEquals("Retrieved corpusId does not match the expected value",id.toLowerCase(),corpusId.toString().toLowerCase());
	}
	
	@Test
	public void testGetCorpusId(){
		
		String corpusId = testLog.getCorpusId();
		assertNotEquals("CorpusId should not be empty, as a default one is generated","", corpusId);
		
		String id = UUID.randomUUID().toString();
		assertEquals("Expected 2 attributes",2, testLog.getAttributes().size());
		testLog.setCorpusId(id);
		assertEquals("There should be exactly 2 attributes",2, testLog.getAttributes().size());
		
		corpusId = testLog.getCorpusId();

		assertNotEquals("Retrieved corpusId should contain a valid UUID value","",corpusId);
		assertEquals("Retrieved corpusId does not match the expected value",id.toLowerCase(),corpusId.toLowerCase());
	}

	@Test
	public void testLength(){
		assertEquals("Expected an empty log",0,testLog.length());
	}
	
	@Test
	public void testXStreamSerialization(){
		XStream xstream = new XStream();
		XesXStreamPersistency.register(xstream);
		//assertEquals(true, converter.canConvert(PaliaLog.class));
		
		System.out.println(xstream.toXML(testLog));
	}
	
	@Test
	public void testMetadata(){
		assertNotNull(testLog.getAttributes().get(PaliaLog.METADATA));
		
		// A non existing metadata attribute should return null
		assertEquals("Expected null, but got something different", null, testLog.getMetadataAttribute("imaginaryKey"));
		
		// Put a String metadata object
		assertEquals(null,testLog.setMetadataAttribute("stringObject", "abc"));
		String oldStringValue = testLog.setMetadataAttribute("stringObject", "def");
		assertNotNull(oldStringValue);
		assertEquals("abc", oldStringValue);
		assertEquals(true, testLog.getMetadataAttribute("stringObject") instanceof XAttributeLiteral);
		assertEquals("def", ((XAttributeLiteral)testLog.getMetadataAttribute("stringObject")).getValue());
		// Put an Integer metadata object
		assertEquals(null,testLog.setMetadataAttribute("intObject", 123));
		int oldIntValue = testLog.setMetadataAttribute("intObject", 456);
		assertNotNull(oldIntValue);
		assertEquals(123, oldIntValue);
		assertEquals(true, testLog.getMetadataAttribute("intObject") instanceof XAttributeDiscrete);
		assertEquals(456, ((XAttributeDiscrete)testLog.getMetadataAttribute("intObject")).getValue());
		// Put an Long metadata object
		assertEquals(null,testLog.setMetadataAttribute("longObject", 123l));
		long oldLongValue = testLog.setMetadataAttribute("longObject", 456l);
		assertNotNull(oldLongValue);
		assertEquals(123l, oldLongValue);
		assertEquals(true, testLog.getMetadataAttribute("longObject") instanceof XAttributeDiscrete);
		assertEquals(456, ((XAttributeDiscrete)testLog.getMetadataAttribute("longObject")).getValue());
		// Put a float metadata Object
		assertEquals(null,testLog.setMetadataAttribute("floatObject", 2.2f));
		float oldFloatValue = testLog.setMetadataAttribute("floatObject", 5.89f);
		assertNotNull(oldFloatValue);
		assertEquals(2.2f, oldFloatValue, 0.01);
		assertEquals(true, testLog.getMetadataAttribute("floatObject") instanceof XAttributeContinuous);
		assertEquals(5.89f, ((XAttributeContinuous)testLog.getMetadataAttribute("floatObject")).getValue(), 0.01);
		// Put a double metadata Object
		assertEquals(null,testLog.setMetadataAttribute("doubleObject", 2.2));
		double oldDoubleValue = testLog.setMetadataAttribute("doubleObject", 6.6666666);
		assertNotNull(oldDoubleValue);
		assertEquals(2.2, oldDoubleValue, 0.01);
		assertEquals(true, testLog.getMetadataAttribute("doubleObject") instanceof XAttributeContinuous);
		assertEquals(6.6666666, ((XAttributeContinuous)testLog.getMetadataAttribute("doubleObject")).getValue(), 1e-6);
		// Put a boolean metadata Object
		assertEquals(null,testLog.setMetadataAttribute("booleanObject", true));
		boolean oldBooleanValue = testLog.setMetadataAttribute("booleanObject", false);
		assertNotNull(oldBooleanValue);
		assertEquals(true, oldBooleanValue);
		assertEquals(true, testLog.getMetadataAttribute("booleanObject") instanceof XAttributeBoolean);
		assertEquals(false, ((XAttributeBoolean)testLog.getMetadataAttribute("booleanObject")).getValue());
		// Put a date metadata object
		Date now = new Date();
		assertEquals(null,testLog.setMetadataAttribute("dateObject", now));
		Date oldDateValue = testLog.setMetadataAttribute("dateObject", new Date());
		assertNotNull(oldDateValue);
		assertEquals(now, oldDateValue);
		assertEquals(true, testLog.getMetadataAttribute("dateObject") instanceof XAttributeTimestamp);
		// Put an UUID metadata object
		assertEquals(null,testLog.setMetadataAttribute("UUIDObject", UUID.randomUUID()));
		assertNotNull(testLog.setMetadataAttribute("UUIDObject", UUID.randomUUID()));
		assertEquals(true, testLog.getMetadataAttribute("UUIDObject") instanceof XAttributeID);
		// Put a container metadata object 
		assertEquals(null,testLog.setMetadataAttribute("containerObject", new HashMap<String, XAttribute>())); 
		assertEquals(true, testLog.getMetadataAttribute("containerObject") instanceof XAttributeContainer);
		// Put a list metadata object
		assertEquals(null,testLog.setMetadataAttribute("listObject", new ArrayList<XAttribute>())); 
		assertEquals(true, testLog.getMetadataAttribute("listObject") instanceof XAttributeList);
		
		TestUtils.printLog(testLog);
	}
}
