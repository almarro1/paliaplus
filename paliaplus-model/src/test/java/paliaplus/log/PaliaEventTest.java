package paliaplus.log;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.deckfour.xes.id.XID;
import org.deckfour.xes.model.XAttributeID;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.out.XSerializer;
import org.deckfour.xes.out.XesXmlSerializer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import paliaplus.log.PaliaEvent;
import paliaplus.log.PaliaLog;
import paliaplus.log.PaliaSample;

public class PaliaEventTest {

	PaliaEvent testEvent;
	XSerializer serializer;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testEvent = new PaliaEvent();
		assertNotNull(testEvent);
		assertEquals("Attributes maps should be empty",0, testEvent.getAttributes().size());
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test
	public void test() {
		serializer = new XesXmlSerializer();
		testEvent.setStartTime(new Date());
		PaliaLog log = new PaliaLog();
		PaliaSample sample = new PaliaSample();
		sample.add(testEvent);
		log.add(sample);
		try{
			serializer.serialize(log, System.out);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testId(){
		assertEquals("Attributes maps should be empty",0, testEvent.getAttributes().size());
		UUID id = UUID.randomUUID();
		testEvent.setId(id.toString()); // should not store it on the attributes map
		assertEquals("Attributes maps should be empty",0, testEvent.getAttributes().size());
		
		assertEquals("Expected a different id",id.toString().toUpperCase(), testEvent.getId());
		
		// check that getId and getID return the same object
		assertEquals("Ids retrieved by different methods don't match",testEvent.getId(), testEvent.getID().toString());
	}
	
	@Test
	public void testStartDate(){
		
		assertNull("Start time was not yet set, method should return null",testEvent.getStartTime());
		
		Date now = new Date();
		
		testEvent.setStartTime(now);
		
		attributesNumber(1);
		
		assertEquals("Timestamps don't match",now.getTime(), testEvent.getStartTime().getTime());
		Date retrievedStartTime = ((XAttributeTimestamp)testEvent.getAttributes().get(PaliaEvent.START_TIME)).getValue();
		assertNotNull(retrievedStartTime);
		assertEquals(now.getTime(), retrievedStartTime.getTime());
	}
	
	@Test
	public void testSampleId(){
		
		assertEquals("SampleId was not set yet, should be an empty string","",testEvent.getSampleId());
		attributesNumber(0);
		UUID id = UUID.randomUUID();
		testEvent.setSampleId(id.toString()); 
		attributesNumber(1);
		
		assertEquals("Expected a different id",id.toString().toUpperCase(), testEvent.getSampleId());
		

		XID sampleId = ((XAttributeID)testEvent.getAttributes().get(PaliaEvent.SAMPLE_ID)).getValue();
		assertEquals("Ids retrieved by different methods don't match",id.toString().toUpperCase(),sampleId.toString());
	}
	
	@Test
	public void testName(){
		
		assertEquals("Name was not set yet, should be an empty string","",testEvent.getName());
		attributesNumber(0);
		String name = "abcde";
		testEvent.setName(name);
		attributesNumber(1);
		
		assertEquals("Expected a different name",name, testEvent.getName());
		

		String name2 = ((XAttributeLiteral)testEvent.getAttributes().get(PaliaEvent.NAME)).getValue();
		assertEquals("Name retrieved by different methods don't match",name,name2);
	}
	
	protected void attributesNumber(final int number){
		assertEquals("Attributes maps should have "+number+" elements",number, testEvent.getAttributes().size());
	}

}
