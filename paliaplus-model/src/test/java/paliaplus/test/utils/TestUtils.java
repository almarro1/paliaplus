package paliaplus.test.utils;

import java.io.IOException;

import org.deckfour.xes.model.XLog;
import org.deckfour.xes.out.XSerializer;
import org.deckfour.xes.out.XesXmlSerializer;

public class TestUtils {
	
	public static void printLog(XLog log){
		XSerializer serializer = new XesXmlSerializer();
		try{
			serializer.serialize(log, System.out);
		}catch(IOException e){
			// Nothing yet
		}
	}
	
	/**
	 * Obtains the memory used.
	 * @return the memory used in KB
	 */
	public static double getMemUsage(){
		double use = (double) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024;
		System.out.println("KB: " + use);
		return use;
	}
}
