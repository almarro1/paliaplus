package paliaplus.tpa;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MetadataObjectTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void instantiateInteger() {
		MetadataObject<Integer> intObj = new MetadataObject<Integer>(Integer.class, 123456);
		assertNotNull(intObj);
		assertNotNull(intObj.value);
		assertEquals(123456,  intObj.value);

	}
}
