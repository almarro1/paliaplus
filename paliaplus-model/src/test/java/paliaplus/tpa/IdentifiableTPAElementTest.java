package paliaplus.tpa;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class IdentifiableTPAElementTest {
	
	class TestElement extends IdentifiableTPAElement {
		protected String field1 = "abc";
		protected int field2 = 234;
		@SuppressWarnings("unused")
		private boolean field3 = false;
	}
	
	TestElement t;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		t = new TestElement();
		assertNotNull(t);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGetMethod(){
		// if we ask for field1, it should give a string with value 'abc'
		String s = (String)t.get("field1", String.class);
		assertNotNull(s);
		assertEquals("abc", s);
		
		// if we ask for field3, it should give a integer with value '234'
		int i = t.get("field2", Integer.class);
		assertNotNull(i);
		assertEquals(234, i);
				
		// if we ask for field3, should return a null value, as it is private
		Object b = t.get("field3", Boolean.class);
		assertNull(b);
	}
}
