package paliaplus.log;

import java.util.Date;
import java.util.UUID;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.id.XID;
import org.deckfour.xes.id.XIDFactory;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeBoolean;
import org.deckfour.xes.model.impl.XAttributeBooleanImpl;
import org.deckfour.xes.model.impl.XAttributeIDImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.deckfour.xes.model.impl.XEventImpl;

public class PaliaEvent extends XEventImpl {
	
	//name
	public static final String NAME = "concept:name";
	//time
	public static final String START_TIME = "time:timestamp";
	
	public static final String SAMPLE_ID = "sampleId";
	
	public static final String IS_START_EVENT = "isStart";
	
	public static final String IS_END_EVENT = "isEnd";
	
	protected XExtension lifeCycle = XLifecycleExtension.instance();
	
	public PaliaEvent(){
		super();
		// Generate a default id
		this.setID(XIDFactory.instance().createId());
	}
	
	public void setName(final String name){
		this.getAttributes().put(NAME, new XAttributeLiteralImpl(NAME, name));
	}
	
	public String getName(){
		XAttribute _name = this.getAttributes().get(NAME);
		if(_name != null){
			return _name.toString();
		}else{
			return "";
		}
	}
	
	public void setStartTime(final Date time){
		this.getAttributes().put(START_TIME, new XAttributeTimestampImpl(START_TIME, time, lifeCycle));
	}
	
	public Date getStartTime(){
		XAttribute startDate = this.getAttributes().get(START_TIME);
		if(startDate != null){
			return ((XAttributeTimestampImpl)startDate).getValue();
		}else{
			return null;
		}
	}
	
	public void setId(final String id){
		this.setID(new XID(UUID.fromString(id)));
	}
	
	public String getId(){
		return this.getID().toString();
	}
	
	public void setSampleId(final String sampleId){
		this.getAttributes().put(SAMPLE_ID, new XAttributeIDImpl(SAMPLE_ID, new XID(UUID.fromString(sampleId))));
	}
	
	public String getSampleId(){
		XAttribute sId = this.getAttributes().get(SAMPLE_ID);
		if(null != sId){
			return ((XAttributeIDImpl)sId).toString();
		}else{
			return "";
		}
	}
	
	public void setIsEnd(final boolean isEnd){
		this.getAttributes().put(IS_END_EVENT, new XAttributeBooleanImpl(IS_END_EVENT,isEnd));
	}
	
	public boolean isEnd(){
		return getBooleanAttribute(IS_END_EVENT);
	}
	
	public void setIsStart(final boolean isStart){
		this.getAttributes().put(IS_START_EVENT, new XAttributeBooleanImpl(IS_START_EVENT,isStart));
	}
	
	public boolean isStart(){
		return getBooleanAttribute(IS_START_EVENT);
	}
	
	protected boolean getBooleanAttribute(final String key){
		XAttributeBoolean value = ((XAttributeBoolean)this.getAttributes().get(key));
		if(null != value){
			return value.getValue();
		}else {
			return false;
		}
	}
}
