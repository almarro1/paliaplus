package paliaplus.log;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.deckfour.xes.model.XAttribute;

public interface MetadataAttributeInterface {
	
	public Long setMetadataAttribute(final String key, long value);
	public Integer setMetadataAttribute(final String key, int value);
	public Double setMetadataAttribute(final String key, double value);
	public Float setMetadataAttribute(final String key, float value);
	public String setMetadataAttribute(final String key, String value);
	public Boolean setMetadataAttribute(final String key, boolean value);
	public UUID setMetadataAttribute(final String key, UUID value);
	public Date setMetadataAttribute(final String key, Date value);
	public Map<String, XAttribute> setMetadataAttribute(final String key,final Map<String, XAttribute> value);
	public List<XAttribute> setMetadataAttribute(final String key,final List<XAttribute> value);
	
	public Object getMetadataAttribute(final String key);

}
