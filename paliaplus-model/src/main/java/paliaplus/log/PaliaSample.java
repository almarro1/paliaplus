package paliaplus.log;

import java.util.UUID;

import org.deckfour.xes.id.XID;
import org.deckfour.xes.id.XIDFactory;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeID;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.impl.XAttributeIDImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XTraceImpl;

public class PaliaSample extends XTraceImpl {
	
	private static final long serialVersionUID = -9126076080163877733L;
	
	public static final String ID = "id";
	public static final String DESCRIPTION = "description";
	
	public PaliaSample(XAttributeMap attributeMap) {
		super(attributeMap);
	}

	public PaliaSample(){
		this(new XAttributeMapImpl());
		this.getAttributes().put(ID, new XAttributeIDImpl(ID, XIDFactory.instance().createId()));
	}
	
	/**
	 * Sets the value of the id attribute
	 * @param id the unique id for this sample. Must be a compliant {@link UUID} string 
	 */
	public void setId(final String id){
		XAttributeID _id = new XAttributeIDImpl(ID, new XID(UUID.fromString(id)));
		this.getAttributes().put(ID, _id);
	}
	
	/**
	 * Returns the value of the id attribute for this sample
	 * @return the compliant {@link UUID} value if the attribute is set, <code>""</code> otherwise
	 */
	public String getId(){
		XAttribute id = this.getAttributes().get(ID);
		if(null!=id){
			return id.toString();
		}else{
			return "";
		}
	}

	public String getDescription(){
		XAttribute description = this.getAttributes().get(DESCRIPTION);
		if(null!=description){
			return description.toString();
		}else{
			return "";
		}
	}
	
	public void setName(final String name){
		XAttributeLiteral _name = new XAttributeLiteralImpl("concept:name", name);
		this.getAttributes().put("concept:name", _name);
	}
	
	public String getName(){
		XAttribute name = this.getAttributes().get("concept:name");
		if(null!=name){
			return name.toString();
		}else{
			return "";
		}
	}
	
	public void setDescription(final String description){
		XAttributeLiteral desc = new XAttributeLiteralImpl(DESCRIPTION, description);
		this.getAttributes().put("description", desc);
	}
	
	public void addActivity(final PaliaEvent activity){
		this.add(activity);
	}
	
	public PaliaEvent getActivity(){
		return null;
	}
}
