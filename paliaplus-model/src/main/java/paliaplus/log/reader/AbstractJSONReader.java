package paliaplus.log.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonReader;

import paliaplus.log.PaliaLog;

public abstract class AbstractJSONReader {
	
	protected JsonReader reader;
	
	public <T extends PaliaLog> T parse(String fileName) throws FileNotFoundException{
		File f = new File(fileName);
		return this.parse(f);
	}
	
	public <T extends PaliaLog> T parse(File f) throws FileNotFoundException{
		InputStream is = new FileInputStream(f);
		T l = this.parse(is);
		try {
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}
	
	public <T extends PaliaLog> T parse(InputStream in){
		reader = Json.createReader(in);
		T l = this.process(reader);
		reader.close();
		return l;
	}

	protected abstract <T extends PaliaLog> T process(JsonReader reader);
}
