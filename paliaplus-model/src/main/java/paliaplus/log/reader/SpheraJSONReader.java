package paliaplus.log.reader;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;


import paliaplus.log.PaliaActivity;
import paliaplus.log.PaliaLog;
import paliaplus.log.PaliaSample;

public class SpheraJSONReader extends AbstractJSONReader {
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends PaliaLog> T process(JsonReader reader) {
		PaliaLog log = new PaliaLog();
		JsonObject logObject = reader.readObject();
		reader.close();
		// Map CorpusId to the name attribute
		JsonString corpusName = (JsonString)logObject.get("CorpusID");
		log.setName(corpusName.getString());
		// Read metadata
		
		// Read samples
		JsonObject samples = logObject.getJsonObject("Samples");
		for(JsonValue sample : samples.values()){
			JsonReader sampReader = Json.createReader(new StringReader(sample.toString()));
			JsonObject tempObj = sampReader.readObject();
			sampReader.close();
			
			PaliaSample logSample = new PaliaSample();
			// Set Id
			logSample.setId(tempObj.getString("Id"));
			// Map SampleId to name attribute
			logSample.setName(tempObj.getString("SampleId"));
			// Read Metadata
			
			// TODO: Map 'IsPositiveSample' to ....
			
			// Read activities
			JsonArray activities = tempObj.getJsonArray("Activities");
			Iterator<JsonValue> it = activities.iterator();
			while(it.hasNext()){
				JsonValue tempActivity = it.next();
				JsonReader actReader = Json.createReader(new StringReader(tempActivity.toString()));
				JsonObject activity = actReader.readObject();
				reader.close();
				PaliaActivity logActivity = new PaliaActivity();
				logActivity.setSampleId(logSample.getId());
				
				// Set start and end time
				try {
					logActivity.setStartTime(parseDate(activity.getString("Start")));
					logActivity.setEndTime(parseDate(activity.getString("End")));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				// Set Name
				logActivity.setName(activity.getString("ActivityName"));
				// TODO: Set Literal
				// "Literal": null,
				
				// IsEnd, IsStart flags
				logActivity.setIsStart(activity.getBoolean("IsStartActivity"));
				logActivity.setIsEnd(activity.getBoolean("IsEndActivity"));
				
				// TODO: Set Metadata
				//				"MetaData": {
				//				},
				
				// TODO: Set Properties
				//				"Properties": [
				//				],
				
				// TODO: Set info
				//				"info": null,
				
				// TODO: Set Results. Fix this mapping
				List<String> results = new ArrayList<String>();
				for(JsonValue v : activity.getJsonArray("Results")){
					results.add(v.toString());
				}
				logActivity.setResults(results);
				
				logSample.add(logActivity);
			}
			log.addSample(logSample);
		}
		
		
		return (T)log;
	}
	
	protected Date parseDate(String dateString) throws ParseException{
		if(!dateString.contains(".")){
			dateString += ".000";
		}
		return df.parse(dateString);
	}

}
