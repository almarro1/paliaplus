package paliaplus.log;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeList;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.impl.XAttributeListImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;

public class PaliaActivity extends PaliaEvent {
	
	public static String END_TIME = "lifecycle:complete";
	public static String RESULTS = "results";
	
	public PaliaActivity(){
		super();
	}
	
	public void setEndTime(final Date end){
		this.getAttributes().put(END_TIME, new XAttributeTimestampImpl(END_TIME, end, XLifecycleExtension.instance()));
	}
	
	public Date getEndTime(){
		XAttribute endDate = this.getAttributes().get(END_TIME);
		if(endDate != null){
			return ((XAttributeTimestampImpl)endDate).getValue();
		}else{
			return null;
		}
	}
	
	public void setResults(List<String> results){
		XAttributeList resultsAttr = (XAttributeList)this.getAttributes().get(RESULTS);
		if(null == resultsAttr){
			resultsAttr = new  XAttributeListImpl(RESULTS);
		}
		for(String r: results){
			resultsAttr.getAttributes().put("", new XAttributeLiteralImpl(RESULTS, r));
			//resultsAttr.addToCollection(new XAttributeLiteralImpl(RESULTS, r));
		}
		this.getAttributes().put(RESULTS, resultsAttr);
	}
	
	public List<String> getResults(){
		XAttributeList results = (XAttributeList)this.getAttributes().get(RESULTS);
		List<String> list = null;
		if(null != results){
			list = new ArrayList<String>();
			Iterator<XAttribute> it = results.getCollection().iterator();
			while(it.hasNext()){
				XAttributeLiteral result = (XAttributeLiteral)it.next();
				list.add(result.getValue());
			}
		}
		return list;
	}
}
