package paliaplus.log;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XIdentityExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XOrganizationalExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.id.XID;
import org.deckfour.xes.id.XIDFactory;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeID;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.impl.XAttributeBooleanImpl;
import org.deckfour.xes.model.impl.XAttributeIDImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XAttributeMapLazyImpl;
import org.deckfour.xes.model.impl.XLogImpl;

public class PaliaLog extends XLogImpl implements MetadataAttributeInterface {

	private static final long serialVersionUID = -5202897011390738475L;
	
	public static final String CORPUS_ID = "identity:id";
	
	public static final String CORPUS_NAME = "concept:name";
	
	public static final String METADATA = "metadata";

	/**
	 * Initializes an empty log instance
	 */
	public PaliaLog(){
		this(new XAttributeMapLazyImpl<XAttributeMapImpl>(XAttributeMapImpl.class));
	}
	
	public PaliaLog(XAttributeMap attributeMap) {
		super(attributeMap);	
		
		// Generate a default corpusId
		this.getAttributes().put(CORPUS_ID, new XAttributeIDImpl(CORPUS_ID, XIDFactory.instance().createId()));
		
		// Create a metadata object
		MetadataHelper.createMetadataObject(this);
		
		// Register LifeCycle extension
		this.getExtensions().add(XLifecycleExtension.instance());
		// Register Concept extension
		this.getExtensions().add(XConceptExtension.instance());
		// Register time extension
		this.getExtensions().add(XTimeExtension.instance());
		// Register Organizational extension
		this.getExtensions().add(XOrganizationalExtension.instance());
		// Register Identity extension
		this.getExtensions().add(XIdentityExtension.instance());
		
		// Define Globals
		this.getGlobalEventAttributes().add(new XAttributeLiteralImpl(PaliaEvent.NAME, "", XConceptExtension.instance()));
		this.getGlobalEventAttributes().add(new XAttributeLiteralImpl("org:resource", "", XOrganizationalExtension.instance()));
		this.getGlobalEventAttributes().add(new XAttributeLiteralImpl(PaliaEvent.SAMPLE_ID, ""));
		this.getGlobalEventAttributes().add(new XAttributeBooleanImpl(PaliaEvent.IS_START_EVENT, false));
		this.getGlobalEventAttributes().add(new XAttributeBooleanImpl(PaliaEvent.IS_END_EVENT, false));
	}
	
	/**
	 * Sets the value of the corpusId attribute
	 * @param corpusId the unique id for this log. Must be a compliant {@link UUID} string 
	 */
	public void setCorpusId(final String corpusId){
		XAttributeID _corpusId = new XAttributeIDImpl(CORPUS_ID, new XID(UUID.fromString(corpusId)));
		this.getAttributes().put(CORPUS_ID, _corpusId);
	}
	
	/**
	 * Sets the value of the name attribute
	 * @param name the namefor this log.
	 */
	public void setName(final String name){
		XAttributeLiteral _name = new XAttributeLiteralImpl(CORPUS_NAME, name);
		this.getAttributes().put(CORPUS_NAME, _name);
	}
	
	/**
	 * Returns the value of the corpusId attribute for this log
	 * @return the compliant {@link UUID} value if the attribute is set, <code>""</code> otherwise
	 */
	public String getCorpusId(){
		XAttribute corpusId = this.getAttributes().get(CORPUS_ID);
		if(null!=corpusId){
			return corpusId.toString();
		}else{
			return "";
		}
	}

	/**
	 * Returns the number of samples in the log
	 * @return
	 */
	public long length(){
		return this.size();
	}
	
	public void addSample(final PaliaSample sample){
		this.add(sample);
	}
	
	public PaliaSample getSample(){
		return null;
	}

	@Override
	public Long setMetadataAttribute(final String key, final long value) {
		return MetadataHelper.setLongValue(this, key, value);
	}

	@Override
	public Integer setMetadataAttribute(final String key, final int value) {
		return MetadataHelper.setIntValue(this, key, value);
	}

	@Override
	public Double setMetadataAttribute(final String key, final double value) {
		return MetadataHelper.setDoubleValue(this, key, value);
	}

	@Override
	public Float setMetadataAttribute(final String key, final float value) {
		return MetadataHelper.setFloatValue(this, key, value);
	}

	@Override
	public String setMetadataAttribute(final String key, final String value) {
		return MetadataHelper.setStringValue(this, key, value);
	}

	@Override
	public Boolean setMetadataAttribute(final String key, final boolean value) {
		return MetadataHelper.setBooleanValue(this, key, value);
	}

	@Override
	public UUID setMetadataAttribute(final String key, final UUID value) {
		return MetadataHelper.setUUIDValue(this, key, value);
	}

	@Override
	public Date setMetadataAttribute(final String key, final Date value) {
		return MetadataHelper.setDateValue(this, key, value);
	}

	@Override
	public Object getMetadataAttribute(final String key) {
		if(MetadataHelper.hasAttribute(this, key)){
			return MetadataHelper.get(this, key);
		}else{
			return null;
		}
	}

	@Override
	public Map<String, XAttribute> setMetadataAttribute(String key, Map<String, XAttribute> value) {
		return MetadataHelper.setContainerValue(this, key, value);
	}

	@Override
	public List<XAttribute> setMetadataAttribute(String key, List<XAttribute> value) {
		return (List<XAttribute>)MetadataHelper.setListValue(this, key, value);
	}
	
	
}
