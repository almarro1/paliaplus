package paliaplus.log;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.deckfour.xes.id.XID;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeBoolean;
import org.deckfour.xes.model.XAttributeContinuous;
import org.deckfour.xes.model.XAttributeDiscrete;
import org.deckfour.xes.model.XAttributeID;
import org.deckfour.xes.model.XAttributeList;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XElement;
import org.deckfour.xes.model.impl.XAttributeBooleanImpl;
import org.deckfour.xes.model.impl.XAttributeContainerImpl;
import org.deckfour.xes.model.impl.XAttributeContinuousImpl;
import org.deckfour.xes.model.impl.XAttributeDiscreteImpl;
import org.deckfour.xes.model.impl.XAttributeIDImpl;
import org.deckfour.xes.model.impl.XAttributeListImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;

public class MetadataHelper {

	@SuppressWarnings("unchecked")
	protected synchronized static <T extends XAttribute> T setMetadataObject(XElement element, final String key,
			Object value, Class<T> type) {
		XAttribute mo = null;
		
		// Get all public constructors for 'type'
		Constructor<?>[] constructors = type.getConstructors();
		int i = 0, l= constructors.length;
		do {
			try {
				// We need a constructor with exactly 2 arguments
				if(constructors[i].getGenericParameterTypes().length != 2){
					// if it does not use 2 params, continue searching
					i++;
					continue;
				}
				// attempt instantiating the class
				mo = ((T) (constructors[i++]).newInstance(key, value));

			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | SecurityException e) {
				// The constructor was not compatible with the parameters used, maybe next one does, so keep trying
				// TODO: nothing to do, maybe log the error with 'trace' verbosity
			}
		} while (mo == null && i<l);

		// Check if element contains metadata attributes
		if (!hasMetadata(element)) {
			// create if not initialized
			createMetadataObject(element);
		}
		// Check if there is a previous metadata object with that key
		return (T) element.getAttributes().get("metadata").getAttributes().put(key, mo);

	}

	protected synchronized static void createMetadataObject(XElement element) {
		if (!hasMetadata(element)) {
			element.getAttributes().put("metadata", new XAttributeContainerImpl("metadata"));
		}
	}

	protected synchronized static boolean hasMetadata(XElement element) {
		return element.getAttributes().containsKey("metadata");
	}

	public static Long setLongValue(XElement element, final String key, long value) {
		XAttribute attr = setMetadataObject(element, key, value, XAttributeDiscreteImpl.class);
		if (null != attr) {
			return ((XAttributeDiscrete) attr).getValue();
		} else {
			return null;
		}
	}

	public static Integer setIntValue(XElement element, final String key, int value) {
		XAttribute attr = setMetadataObject(element, key, value, XAttributeDiscreteImpl.class);
		if (null != attr) {
			return (int) (((XAttributeDiscrete) attr).getValue());
		} else {
			return null;
		}
	}

	public static Float setFloatValue(XElement element, final String key, float value) {
		XAttribute attr = setMetadataObject(element, key, value, XAttributeContinuousImpl.class);
		if (null != attr) {
			return (float) (((XAttributeContinuous) attr).getValue());
		} else {
			return null;
		}
	}

	public static Double setDoubleValue(XElement element, final String key, double value) {
		XAttribute attr = setMetadataObject(element, key, value, XAttributeContinuousImpl.class);
		if (null != attr) {
			return ((XAttributeContinuous) attr).getValue();
		} else {
			return null;
		}
	}

	public static String setStringValue(XElement element, final String key, String value) {
		XAttribute attr = setMetadataObject(element, key, value, XAttributeLiteralImpl.class);
		if (null != attr) {
			return ((XAttributeLiteral) attr).getValue();
		} else {
			return null;
		}
	}

	public static Boolean setBooleanValue(XElement element, final String key, boolean value) {
		XAttribute attr = setMetadataObject(element, key, value, XAttributeBooleanImpl.class);
		if (null != attr) {
			return ((XAttributeBoolean) attr).getValue();
		} else {
			return null;
		}
	}

	public static UUID setUUIDValue(XElement element, final String key, UUID value) {
		XAttribute attr = setMetadataObject(element, key, new XID(value), XAttributeIDImpl.class);
		if (null != attr) {
			return UUID.fromString(((XAttributeID) attr).getValue().toString());
		} else {
			return null;
		}
	}

	public static Date setDateValue(XElement element, final String key, Date value) {
		XAttribute attr = setMetadataObject(element, key, value, XAttributeTimestampImpl.class);
		if (null != attr) {
			return ((XAttributeTimestamp) attr).getValue();
		} else {
			return null;
		}
	}

	public static Map<String, XAttribute> setContainerValue(XElement element, final String key,
			Map<String, XAttribute> value) {
		// Check if metadata contains the container
		Map<String, XAttribute> oldContainer = null;
		if (!hasAttribute(element, key)) {
			element.getAttributes().get("metadata").getAttributes().put(key, new XAttributeContainerImpl(key));
		} else {
			oldContainer = element.getAttributes().get("metadata").getAttributes().get(key).getAttributes();
		}
		element.getAttributes().get("metadata").getAttributes().get(key).setAttributes(new XAttributeMapImpl(value));

		return oldContainer;
	}

	public static List<XAttribute> setListValue(XElement element, final String key, List<XAttribute> value) {
		// Create the list
		XAttributeList list = new XAttributeListImpl(key);
		for (XAttribute attr : value) {
			list.addToCollection(attr);
		}
		XAttributeList oldList = (XAttributeList) element.getAttributes().get("metadata").getAttributes().put(key,
				list);

		if (null != oldList) {
			return new ArrayList<XAttribute>(oldList.getCollection());
		} else {
			return null;
		}
	}

	public static boolean hasAttribute(XElement element, String key) {
		if (hasMetadata(element)) {
			return element.getAttributes().get("metadata").getAttributes().containsKey(key);
		} else {
			return false;
		}
	}

	public static Object get(XElement element, String key) {
		if (hasMetadata(element)) {
			return element.getAttributes().get("metadata").getAttributes().get(key);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T getAs(XElement element, String key, Class<T> type) {
		return (T) get(element, key);
	}
}
