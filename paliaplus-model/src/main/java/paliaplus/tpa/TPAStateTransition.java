package paliaplus.tpa;


public class TPAStateTransition extends IdentifiableTPAElement {
	
	protected String name;

	protected String sourceState;
	protected String destinationState;

	protected String associatedNodeTransition;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSourceState() {
		return sourceState;
	}

	public void setSourceState(String sourceState) {
		this.sourceState = sourceState;
	}

	public String getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public String getAssociatedNodeTransition() {
		return associatedNodeTransition;
	}

	public void setAssociatedNodeTransition(String associatedNodeTransition) {
		this.associatedNodeTransition = associatedNodeTransition;
	}
}
