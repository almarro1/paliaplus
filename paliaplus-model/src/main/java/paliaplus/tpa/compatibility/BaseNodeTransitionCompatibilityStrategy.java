package paliaplus.tpa.compatibility;

import paliaplus.tpa.TPANode;
import paliaplus.tpa.TPANodeTransition;

/**
 * Implements a basic node transition compatibility strategy based on three premises:
 * - The transition expression is the same
 * - Source nodes are compatible
 * - End nodes are compatible
 */
public class BaseNodeTransitionCompatibilityStrategy implements NodeTransitionCompatibilityStrategy {

	private NodeCompatibilityStrategy ns;
	
	public BaseNodeTransitionCompatibilityStrategy() {
		ns = new NodeCompatibilityByFieldValue<String>("id", String.class);
	}
		
	@Override
	public boolean check(TPANodeTransition nt1, TPANodeTransition nt2) {
		return nt1.getExpression().equals(nt2.getExpression()) && TPANode.areCompatible(nt1.getSourceNodes(), nt2.getSourceNodes(), ns)
				&& TPANode.areCompatible(nt1.getDestinationNodes(), nt2.getDestinationNodes(), ns); 
	}

}
