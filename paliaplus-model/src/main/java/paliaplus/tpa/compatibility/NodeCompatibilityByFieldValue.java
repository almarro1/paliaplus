package paliaplus.tpa.compatibility;

import paliaplus.tpa.TPANode;

/**
 * Implements a Node Compatibility Strategy in which the value of a field is used to determine the compatibility
 * @author almarro1
 *
 * @param <T> the type of the field that is used to check the compatibility 
 */
public class NodeCompatibilityByFieldValue<T> implements NodeCompatibilityStrategy {

	/**
	 * The name of the class field that will be used to check compatibility
	 */
	protected String attrName; 
	/**
	 * The type of the field used to check compatibility
	 */
	protected Class<T> type;
	
	/**
	 * Creates a new instance of the compatibility checker
	 * @param attrName the name of the field to be used for check
	 * @param type the type of such field
	 */
	public NodeCompatibilityByFieldValue(final String attrName, final Class<T> type) {
		this.attrName = attrName;
		this.type = type;
		
	}

	@Override
	public boolean check(TPANode n1, TPANode n2) {	
		return n1!= null && n2!= null && n1.get(this.attrName, type).equals(n2.get(this.attrName, type));
	}
}
