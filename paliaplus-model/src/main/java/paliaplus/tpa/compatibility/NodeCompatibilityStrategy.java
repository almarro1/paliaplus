package paliaplus.tpa.compatibility;

import paliaplus.tpa.TPANode;

/**
 * Defines the strategy to be used to decide whether 2 TPANodes are compatible
 * @author almarro1
 *
 */
public interface NodeCompatibilityStrategy {
	
	/**
	 * Implements the strategy to decide if two different nodes are compatible to each other
	 * @param n1 the first node
	 * @param n2 the second node
	 * @return <code>true</code> if nodes <code>n1</code> and <code>n2</code> are compatible, <code>false</code> otherwise
	 */
	boolean check(final TPANode n1, final TPANode n2);
}
