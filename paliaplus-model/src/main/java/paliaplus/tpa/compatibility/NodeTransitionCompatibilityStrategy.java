package paliaplus.tpa.compatibility;

import paliaplus.tpa.TPANodeTransition;

/**
 * Defines the strategy to be used to decide whether 2 TPANodes are compatible
 * @author almarro1
 *
 */
public interface NodeTransitionCompatibilityStrategy {
	
	/**
	 * Implements the strategy to decide if two different node transitions are compatible to each other
	 * @param nt1 the first node transition
	 * @param nt2 the second node transition
	 * @return <code>true</code> if node transitions <code>n1</code> and <code>n2</code> are compatible, <code>false</code> otherwise
	 */
	boolean check(final TPANodeTransition nt1, final TPANodeTransition nt2);
}
