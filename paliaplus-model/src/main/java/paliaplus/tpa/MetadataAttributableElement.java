package paliaplus.tpa;

import java.util.HashMap;
import java.util.Map;

public abstract class MetadataAttributableElement extends IdentifiableTPAElement {

	protected Map<String, MetadataObject<?>> metadata = new HashMap<String, MetadataObject<?>>();
	
	public MetadataAttributableElement() {
		super();
	}
	
	public MetadataAttributableElement(final String id) {
		super(id);
	}
	
	public MetadataAttributableElement(final String id, final Map<String, MetadataObject<?>> metadata){
		this(id);
		this.metadata = metadata;
	}
	
	public Map<String, MetadataObject<?>> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, MetadataObject<?>> metadata) {
		this.metadata = metadata;
	}
}