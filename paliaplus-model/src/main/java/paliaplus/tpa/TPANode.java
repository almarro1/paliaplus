package paliaplus.tpa;

import java.util.ArrayList;
import java.util.List;

import paliaplus.tpa.compatibility.NodeCompatibilityStrategy;

public class TPANode extends IdentifiableTPAElement {

	/**
	 * Label that contains a human readable identifier for this node.
	 */
	protected String name;

	/**
	 * Flag indicating whether this node is the first one in the process
	 */
	protected boolean isStartingNode;

	/**
	 * Flag indicating whether this node represents a final step on a process
	 */
	protected boolean isFinalNode;

	protected List<Clock> clocks = new ArrayList<Clock>();
	protected List<Variable> preconditions = new ArrayList<Variable>();

	protected List<Variable> postconditions = new ArrayList<Variable>();

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public boolean isStartingNode() {
		return isStartingNode;
	}

	public void setStartingNode(boolean isStartingNode) {
		this.isStartingNode = isStartingNode;
	}

	public boolean isFinalNode() {
		return isFinalNode;
	}

	public void setFinalNode(boolean isFinalNode) {
		this.isFinalNode = isFinalNode;
	}

	public List<Clock> getClocks() {
		return clocks;
	}

	public void setClocks(List<Clock> clocks) {
		this.clocks = clocks;
	}

	public List<Variable> getPreconditions() {
		return preconditions;
	}

	public void setPreconditions(List<Variable> preconditions) {
		this.preconditions = preconditions;
	}

	public List<Variable> getPostconditions() {
		return postconditions;
	}

	public void setPostconditions(List<Variable> postconditions) {
		this.postconditions = postconditions;
	}
	
	
	/* ***************************
	 *	Helper methods to get transitions 
	 ***************************** */
	/**
	 * Returns the transitions that enter/exit this node
	 * @param tpa the model that contains this node 
	 * @param in flag indicating whether we want entering transitions (<code>true</code>) or 
	 * @param exclusive
	 * @return an array with all the matching transitions
	 */
	protected List<TPANodeTransition> getTransitions(TPATemplate tpa, boolean in, boolean exclusive) throws Exception{
		throw new Exception("Not implemented yet");
		//return null;
	}
	
	/**
	 * Obtains a list of node transitions leaving from the current node
	 * @param tpa
	 * @param exclusive
	 * @return
	 */
	public List<TPANodeTransition> getOutTransitions(TPATemplate tpa, boolean exclusive) {
		List<TPANodeTransition> res = new ArrayList<TPANodeTransition>();
		if (exclusive) {
			return this.getExclusiveOutgoingTransitions(tpa);
		} else {
			for (TPANodeTransition nt : tpa.getNodeTransitions().values()) {
				if (nt.sourceNodes.contains(this.id)) {
					res.add(nt);
				}
			}
		}
		return res;
	}

	/**
	 * Obtains the set of incoming transitions to a specific node
	 * @param tpa the TPA instance
	 * @param exclusive flag indicating that transitions should only reach this node
	 * @return
	 */
	public List<TPANodeTransition> getInTransitions(TPATemplate tpa, boolean exclusive) {
		List<TPANodeTransition> res = new ArrayList<TPANodeTransition>();
		if (exclusive) {
			return this.getExclusiveIncomingTransitions(tpa);
		} else {

			for (TPANodeTransition nt : tpa.getNodeTransitions().values()) {
				if (nt.destinationNodes.contains(id))
					res.add(nt);
			}
		}
		return res;
	}
	
	/**
	 * Obtains exclusive transitions arriving to this node
	 * @param tpa
	 * @return
	 */
	public List<TPANodeTransition> getExclusiveIncomingTransitions(TPATemplate tpa){
		List<TPANodeTransition> transitions = new ArrayList<TPANodeTransition>();
		//Iterate over all transitions...
		for(TPANodeTransition nt : tpa.getNodeTransitions().values()){
			// Get only those with just 1 destination node ...
			if(nt.destinationNodes.size()==1 &&
					// ... being that node the current one
					nt.destinationNodes.get(0).toString().equals(this.id)){
				transitions.add(nt);
			}
		}
		return transitions;
	}
	
	/**
	 * Obtains exclusive transitions leaving from this node
	 * @param tpa
	 * @return
	 */
	public List<TPANodeTransition> getExclusiveOutgoingTransitions(TPATemplate tpa){
		List<TPANodeTransition> transitions = new ArrayList<TPANodeTransition>();
		//Iterate over all transitions...
		for(TPANodeTransition nt : tpa.getNodeTransitions().values()){
			// Get only those with just 1 source node ...
			if(nt.sourceNodes.size()==1 &&
					// ... being that node the current one
					nt.sourceNodes.get(0).toString().equals(this.id)){
				transitions.add(nt);
			}
		}
		return transitions;
	}
	
	/**
	 * Checks if two sets of TPAnode elements are compatible to each other, using a defined strategy to determine whether 
	 * two individual nodes are compatible.
	 * @param nodes1 the first set of nodes to compare
	 * @param nodes2 the second set of node to compare
	 * @param strategy The strategy to decide if two nodes are compatible
	 * @return <code>true</code> if the two sets are compatible, <code>false</code> otherwise
	 */
	public static boolean areCompatible(final List<TPANode> nodes1, final List<TPANode> nodes2, final NodeCompatibilityStrategy strategy){
		// If lists are of different size, then they are not compatible
		if(nodes1.size() != nodes2.size()){
			return false;
		}
		
		// Check node by node for compatibility
		for(TPANode n1 : nodes1){
			for(TPANode n2: nodes2){
				if(!strategy.check(n1,n2)){
					// nodes not compatible, therefore sets are not either
					return false;
				}
			}
		}
		// if we get here, all nodes were compatible
		return true;
	}
}
