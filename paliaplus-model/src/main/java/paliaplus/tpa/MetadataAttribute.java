package paliaplus.tpa;

import java.util.Map;

public class MetadataAttribute<T> extends AbstractMetadataAttribute {
	
	protected T value;
	
	public MetadataAttribute(){
		super();
	}
	
	public MetadataAttribute(final Map<String, MetadataObject<?>> md){
		super(md);
	}
	
	public T getValue(){
		return this.value;
	}
	
	public void setValue(final T value){
		this.value = value;
	}
}
