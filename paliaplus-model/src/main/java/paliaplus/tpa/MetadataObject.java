package paliaplus.tpa;

public class MetadataObject<T extends Object> {
	
	
	protected Class<T> type;
	protected Object value;
	
	public MetadataObject(final Class<T> type, final T value){
		this.value = value;
	}

	public Class<T> getType() {
		return type;
	}

	public void setType(final Class<T> type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(final Object value) {
		this.value = value;
	}
}
