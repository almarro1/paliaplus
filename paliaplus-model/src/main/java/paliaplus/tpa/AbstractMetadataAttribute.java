package paliaplus.tpa;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractMetadataAttribute {
	
	protected Map<String, MetadataObject<?>> metadataCollection;
	
	public AbstractMetadataAttribute(){
		this.metadataCollection = new HashMap<String, MetadataObject<?>>();
	}
	
	public AbstractMetadataAttribute(final Map<String, MetadataObject<?>> md){
		this();
		// Copy all elements in md to metadataCollection
		this.metadataCollection.putAll(md);
	}
	
	public MetadataObject<?> get(final String key){
		return this.metadataCollection.get(key);
	}
	
	public void put(final String key, final MetadataObject<?> value){
		this.metadataCollection.put(key, value);
	}
}
