package paliaplus.tpa;

public class Variable {
	/**
	 * The name of the variable
	 */
	protected String name;
	
	/**
	 * String representing the value of the variable
	 */
	protected String expression;

	public Variable(String name, String expression) {
		super();
		this.name = name;
		this.expression = expression;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}
}
