package paliaplus.tpa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TPATemplate extends MetadataAttributableElement {
	
	protected Map<String, TPANode> nodes = new HashMap<String, TPANode>();
	protected Map<String, TPANodeTransition> nodeTransitions = new HashMap<String, TPANodeTransition>();
	
	protected Map<String, TPAState> states = new HashMap<String, TPAState>();
	protected Map<String, TPAStateTransition> stateTransitions = new HashMap<String, TPAStateTransition>();
	
	
	public TPATemplate(){
		super();
	}

	// **************************
	// Getters and setters
	// **************************
	public Map<String, TPANode> getNodes() {
		return nodes;
	}


	public void setNodes(Map<String, TPANode> nodes) {
		this.nodes = nodes;
	}


	public Map<String, TPANodeTransition> getNodeTransitions() {
		return nodeTransitions;
	}


	public void setNodeTransitions(Map<String, TPANodeTransition> nodeTransitions) {
		this.nodeTransitions = nodeTransitions;
	}


	public Map<String, TPAState> getStates() {
		return states;
	}


	public void setStates(Map<String, TPAState> states) {
		this.states = states;
	}


	public Map<String, TPAStateTransition> getStateTransitions() {
		return stateTransitions;
	}

	public void setStateTransitions(Map<String, TPAStateTransition> stateTransitions) {
		this.stateTransitions = stateTransitions;
	}
	
	
	// ******************************
	// Node manipulation
	// ******************************
	/**
	 * Returns the node whose name attribute matches the given string
	 * @param name
	 * @return
	 */
	public TPANode getNodeByName(final String name){
		for(TPANode node : this.nodes.values()){
			if(node.getName().equals(name)){
				return node;
			}
		}
		return null;
	}
	
	/**
	 * Returns the TPANode with a matching id, <code>null</code> if the id is not registered
	 * @param id
	 * @return
	 */
	public TPANode getNodeById(final String id){
		return this.nodes.get(id);
	}
	
	/**
	 * Returns a list object containing the nodes in the template 
	 * @return
	 */
	public List<TPANode> getNodesAsList(){
		return Arrays.asList(this.getNodesAsArray());
	}
	
	/**
	 * Returns an raw array containing the TPAnodes in the template 
	 * @return
	 */
	public TPANode[] getNodesAsArray(){
		return this.nodes.values().toArray(new TPANode[0]);
	}
	
	/**
	 * Returns a list of TPANodes given a list of string identifiers
	 * @param nodeIds node string identifiers
	 * @return
	 */
	public List<TPANode> resolveNodes(final List<String> nodeIds){
		List<TPANode> _temp = new ArrayList<TPANode>();
		for(String id : nodeIds){
			if(this.nodes.containsKey(id)){
				_temp.add(this.nodes.get(id));
			}
		}
		return _temp;
	}
	
	/**
	 * Returns an array of TPANodes given a list of string identifiers
	 * @param nodeIds node string identifiers
	 * @return
	 */
	public TPANode[] resolveNodesAsArray(final List<String> nodeIds){
		return this.resolveNodes(nodeIds).toArray(new TPANode[0]);
	}
	
	/**
	 * Obtains a list of nodes tagged as 'final' in the template
	 * @return
	 */
	public List<TPANode> getFinalNodes(){
		List<TPANode> nodes = new ArrayList<TPANode>();
		for(TPANode node : this.nodes.values()){
			if(node.isFinalNode()){
				nodes.add(node);
			}
		}
		return nodes;
	}
	
	/**
	 * Obtains a list of nodes tagged as 'start' in the template
	 * @return
	 */
	public List<TPANode> getStartNodes(){
		List<TPANode> nodes = new ArrayList<TPANode>();
		for(TPANode node : this.nodes.values()){
			if(node.isStartingNode()){
				nodes.add(node);
			}
		}
		return nodes;
	}
	
	// ******************************
	// Node transitions manipulation
	// ******************************
	/**
	 * Returns a node transition matching the given id 
	 * @param id
	 * @return
	 */
	public TPANodeTransition getNodeTransitionById(final String id){
		return this.nodeTransitions.get(id);
	}
	
	/**
	 * Returns a list object containing the node transitions in the template 
	 * @return
	 */
	public List<TPANodeTransition> getNodeTransitionsAsList(){
		return Arrays.asList(this.getNodeTransitionsAsArray());
	}
	
	/**
	 * Returns an raw array containing the TPANodesTransitions in the template 
	 * @return
	 */
	public TPANodeTransition[] getNodeTransitionsAsArray(){
		return this.nodeTransitions.values().toArray(new TPANodeTransition[0]);
	}
}
