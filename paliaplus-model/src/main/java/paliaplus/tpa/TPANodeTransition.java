package paliaplus.tpa;

import java.util.ArrayList;
import java.util.List;

public class TPANodeTransition extends IdentifiableTPAElement{
	
	/**
	 * Set of nodes originating this transition
	 */
	protected List<TPANode> sourceNodes = new ArrayList<TPANode>();
	
	/**
	 * Set of nodes resulting from this transition
	 */
	protected List<TPANode> destinationNodes = new ArrayList<TPANode>();

	protected String expression;
	
	public TPANodeTransition(final String id){
		super(id);
	}
	
	public TPANodeTransition(){
		super();
	}

	public List<TPANode> getSourceNodes() {
		return sourceNodes;
	}

	public void setSourceNodes(List<TPANode> sourceNodes) {
		this.sourceNodes = sourceNodes;
	}

	public List<TPANode> getDestinationNodes() {
		return destinationNodes;
	}

	public void setDestinationNodes(List<TPANode> destinationNodes) {
		this.destinationNodes = destinationNodes;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

}
