package paliaplus.tpa;

import java.util.ArrayList;
import java.util.List;


public class TPAState extends IdentifiableTPAElement {

	protected boolean isStartingState;

	protected boolean isFinalState;

	protected String name;

	protected List<String> stateNodes = new ArrayList<String>();

	public TPAState(){
		super();
	}
	
	public TPAState(final String id){
		super(id);
	}

	public boolean isStartingState() {
		return isStartingState;
	}

	public void setStartingState(boolean isStartingState) {
		this.isStartingState = isStartingState;
	}

	public boolean isFinalState() {
		return isFinalState;
	}

	public void setFinalState(boolean isFinalState) {
		this.isFinalState = isFinalState;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getStateNodes() {
		return stateNodes;
	}

	public void setStateNodes(List<String> stateNodes) {
		this.stateNodes = stateNodes;
	}
}
