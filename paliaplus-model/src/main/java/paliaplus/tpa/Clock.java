package paliaplus.tpa;

import java.util.Date;

public class Clock extends IdentifiableTPAElement{

	protected String symbol;
	protected long time;
	
	public Clock() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Clock(final String id) {
		super(id);
	}

	public Clock(final long time, final String symbol) {
		super();
		this.symbol = symbol;
		this.time = time;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	public Date getTimeAsDate(){
		return new Date(this.time);
	}
}
