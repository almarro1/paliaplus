package paliaplus.tpa;

import java.lang.reflect.Field;
import java.util.UUID;

public abstract class IdentifiableTPAElement {
	
	/**
	 * Element Identifier. Should be compatible with UUID syntax
	 */
	protected String id;
	
	public IdentifiableTPAElement(){
		this(UUID.randomUUID().toString());
	}
	
	public IdentifiableTPAElement(final String id){
		this.id = UUID.fromString(id).toString();
	}
	
	public String getId(){
		return this.id;
	}

	public void setId(final String value){
		// Validate that value string is really an UUID
		this.id = UUID.fromString(value).toString();
	}
	
	/**
	 * Returns the value of the field with name 'attrName'
	 * @param attrName the name of the field we want to retrieve the value for
	 * @param type the return type we expect
	 * @return the value of the field, null if the field does not exist or the field has private access
	 */
	public <T> T get(String attrName, Class<T> type){
		try{
			Field f = this.getClass().getDeclaredField(attrName);
			return type.cast(f.get(this));
		}catch(NoSuchFieldException ex){
			// the field does not exist
			return null;
		} catch (IllegalArgumentException e) {
			return null;
		} catch (IllegalAccessException e) {
			// Thrown if the field has private access
			// TODO: reconsider if these fields need to be exposed (will require the call to f.setAccessible(true))
			return null;
		} 
	}
}
